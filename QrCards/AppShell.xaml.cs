﻿using QrCards.View;

namespace QrCards;

public partial class AppShell : Shell
{
	public AppShell()
	{
		InitializeComponent();

		Routing.RegisterRoute(nameof(EmailPage), typeof(EmailPage));
	}
}
