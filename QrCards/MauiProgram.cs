﻿using QrCards.Services;
using QrCards.View;
using QrCards.ViewModel;

namespace QrCards;

public static class MauiProgram
{
	public static MauiApp CreateMauiApp()
	{
		var builder = MauiApp.CreateBuilder();
		builder
			.UseMauiApp<App>()
			.ConfigureFonts(fonts =>
			{
				fonts.AddFont("OpenSans-Regular.ttf", "OpenSansRegular");
				fonts.AddFont("OpenSans-Semibold.ttf", "OpenSansSemibold");
			});

		builder.Services.AddSingleton<MainPage>();
		builder.Services.AddSingleton<MainViewModel>();
		
		builder.Services.AddTransient<ISender, Sender>();
        builder.Services.AddTransient<EmailPage>();
		builder.Services.AddTransient<EmailViewModel>();
		

		return builder.Build();
	}
}
