﻿namespace QrCards.Models;

public partial record User(string Email, string FullName, string Phone);
