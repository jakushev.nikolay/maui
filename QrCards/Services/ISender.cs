﻿using QrCards.Models;

namespace QrCards.Services;

public partial interface ISender
{
    void SendEmail(User user);
}
