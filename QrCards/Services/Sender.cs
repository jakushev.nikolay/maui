﻿using QrCards.Models;

namespace QrCards.Services;

public class Sender : ISender
{
    public async void SendEmail(User user)
    {
        if (!Email.Default.IsComposeSupported) { return; }

        string[] recipients = new[] { user.Email };
        var message = new EmailMessage
        {
            Subject = user.FullName,
            Body = user.Phone,
            BodyFormat = EmailBodyFormat.PlainText,
            To = new List<string>(recipients)
        };

        await Email.Default.ComposeAsync(message);
    }
}
