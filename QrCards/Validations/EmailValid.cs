﻿using Plugin.ValidationRules.Interfaces;
using System.ComponentModel.DataAnnotations;

namespace QrCards.Validations;

internal class EmailValid : IValidationRule<string>
{
    public string ValidationMessage { get; set ; }

    public bool Check(string value)
    {
        return new EmailAddressAttribute().IsValid(value);
    }
}
