using QrCards.ViewModel;

namespace QrCards.View;

public partial class EmailPage : ContentPage
{
	public EmailPage(EmailViewModel viewModel)
	{
		InitializeComponent();
		BindingContext = viewModel;
	}
}