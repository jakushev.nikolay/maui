﻿using QrCards.Services;
using CommunityToolkit.Mvvm.Input;
using CommunityToolkit.Mvvm.ComponentModel;
using QrCards.Validations;
using Plugin.ValidationRules.Extensions;
using Plugin.ValidationRules;
using QrCards.Models;

namespace QrCards.ViewModel;

[QueryProperty("Email", "Email")]
public partial class EmailViewModel : ObservableObject
{
    private readonly ISender _sender;

    [ObservableProperty]
    private Validatable<string> _email;

    [ObservableProperty]
    private Validatable<string> _fio;

    [ObservableProperty]
    private Validatable<string> _phone;

    public EmailViewModel(ISender sender)
    {
        this._sender = sender;

        _fio = Validator.Build<string>()
                          .WithRule(new IsNotNullOrEmptyRule(), "A email is required.");

        _phone = Validator.Build<string>()
                .When(x => !string.IsNullOrEmpty(x))
                .Must(x => x.Length == 12, "Minimum lenght is 12.");

    }

    [RelayCommand]
    public async void SendEmail()
    {
        if (IsValidate())
        {
            _sender.SendEmail(CreateUser());
            await Application.Current.MainPage.Navigation.PopToRootAsync();
        }
    }

    private User CreateUser()
    {
        return new User(Email: this._email.Value, FullName: this._fio.Value, Phone: this._phone.Value);
    }

    private bool IsValidate()
    {
        return _fio.Validate() && _phone.Validate();
    }
}
