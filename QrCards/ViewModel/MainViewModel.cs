﻿using QrCards.View;
using CommunityToolkit.Mvvm.Input;
using CommunityToolkit.Mvvm.ComponentModel;
using Plugin.ValidationRules.Extensions;
using QrCards.Validations;
using Plugin.ValidationRules;

namespace QrCards.ViewModel;

public partial class MainViewModel : ObservableObject
{
    [ObservableProperty]
    private Validatable<string> _email;

    public MainViewModel()
    {
        _email = Validator.Build<string>()
                          .WithRule(new IsNotNullOrEmptyRule(), "A email is required.")
                          .WithRule(new EmailValid(), "Email is not valid");
    }

    [RelayCommand]
    public async void NextPage()
    {
        if (Validate())
        {
            var navigatitionParameter = new Dictionary<string, object>
            {
                { "Email", _email }
            };

            await Shell.Current.GoToAsync(nameof(EmailPage), navigatitionParameter);
        }
    }

    private bool Validate()
    {
        return _email.Validate();
    }

}
